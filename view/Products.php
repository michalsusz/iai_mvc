<?php
require_once 'view/View.php';
require_once 'model/ProductsModel.php';

class ProductsView extends View {

    public function index()
    {
        $model = new ProductsModel();
        $array = $model->getAllData();
        $this->set('products', $array);
        $this->renderTemplate('ProductsTemplate');
    }


}

