<?php

abstract class Model {
    protected $pdo;
    
    public function  __construct() 
    {
        $this->setSQLConnection();
    }
    
    private function setSQLConnection()
    {
        try {
            require 'config/sql.php';
            $dbc['options'] = array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            );
            $this->pdo=new PDO(
                    'mysql:host=' .  SQL_HOST . ';dbname=' . SQL_DBASE, 
                    SQL_USER, 
                    SQL_PASSWORD, $dbc['options']
            );
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(DBException $e) {
            echo 'The connect can not create: ' . $e->getMessage();
            exit;
        }
    }
    
    public function loadModel($name, $path = 'model/') 
    {
        $modelName = $name . 'Model';
        $modelPath = $path . $name . '.php';
        
        try {
            if (!file_exists($modelPath)) {
                throw new Exception('Can not open model ' . $modelName);
            } else {
                require $modelPath;
                $model = new $modelName();
            }
        } catch (Exception $e) {
            $e->getMessage();
            exit;
        }
        
        return $model;
    }



    
    
}
