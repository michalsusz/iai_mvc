<?php
require_once 'Model.php';

class ProductsModel extends Model{

    public function getAllData()
    {
        $result = $this->pdo->prepare("SELECT id, name, price FROM products");
        $result->execute();

        while ($row = $result->fetch()) {
            $array[] = $row['name'] . ' ' . $row['price'];
        }
        return $array;
    }
}

