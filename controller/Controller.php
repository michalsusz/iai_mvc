<?php

abstract class Controller {
    public function loadView($name, $path = 'view/')
    {
        $viewName = $name . 'View';
        $viewPath = $path . $name . '.php';
        
        try {
            if (!file_exists($viewPath)) {
                throw new Exception('Can not open view ' . $viewName);
            } else {
                require $viewPath;
                $view = new $viewName();
            }
        } catch (Exception $e) {
            $e->getMessage();
            exit;
        }
        
        return $view;
    }
    
    public function loadModel($name, $path = 'model/')
    {
        $modelName = $name . 'Model';
        $modelPath = $path . $name . '.php';
        
        try {
            if (!file_exists($modelPath)) {
                throw new Exception('Can not open model ' . $modelName);
            } else {
                require $modelPath;
                $model = new $modelName();
            }
        } catch (Exception $e) {
            $e->getMessage();
            exit;
        }
        
        return $model;
    }
}
