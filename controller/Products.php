<?php
require_once 'controller/Controller.php';


class ProductsController extends Controller{

    public function getProducts()
    {
        $view = $this->loadView('Products');
        $view->index();
    }
}

