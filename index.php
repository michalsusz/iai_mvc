<?php

$controllerPath = (isset($_GET['controller'])) ? ucfirst($_GET['controller']) : 'Index';
$controllerName = (isset($_GET['controller'])) ? ucfirst($_GET['controller']) . 'Controller' : 'IndexController';

$action = (isset($_GET['action'])) ? $_GET['action'] : 'index';

$controllerFile = 'controller/' . $controllerPath . '.php';
if (!file_exists($controllerFile)) {
    echo 'Unexpected controller name: ' . $controllerName;
    exit;
}

require_once $controllerFile;

$controller = new $controllerName();
$controller->$action();

